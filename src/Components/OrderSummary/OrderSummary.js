import Aux from "../../hoc/Ax";
import Button from "../UI/Button/Button";

import React from "react";

export const OrderSummary = (props) => {
  const ingredients = Object.keys(props.ingredients).map((key) => {
    return (
      <li key={key}>
        <span style={{ textTransform: "capitalize" }}> {key} </span>:
        <span> {props.ingredients[key]} </span>
      </li>
    );
  });
  return (
    <Aux>
      <h3>
        <strong>Your Order</strong>
      </h3>
      <p>Your Delicious burger with below ingredients</p>
      <ul>{ingredients}</ul>
      <p>
        <strong>Price: {props.price.toFixed(2)}</strong>
      </p>
      <p>Do you want to checkout?</p>
      <Button btnType="Danger" clicked={props.cancelled}>
        CANCEL
      </Button>
      <Button btnType="Success" clicked={props.continued}>
        CONTINUE
      </Button>
    </Aux>
  );
};

export default OrderSummary;

// const OrderSummary = (props) => {
//   const ingredients = Object.keys(props.ingredients).map((key) => {
//     return (
//       <li key={key}>
//         <span style={{ textTransform: "capitalize" }}> {key} </span>:
//         <span> {props.ingredients[key]} </span>
//       </li>
//     );
//   });

//   return (
//     <Aux>
//       <h3>
//         <strong>Your Order</strong>
//       </h3>
//       <p>Your Delicious burger with below ingredients</p>
//       <ul>{ingredients}</ul>
//       <p>
//         <strong>Price: {props.price.toFixed(2)}</strong>
//       </p>
//       <p>Do you want to checkout?</p>
//       <Button btnType="Danger" clicked={props.cancelled}>
//         CANCEL
//       </Button>
//       <Button btnType="Success" clicked={props.continued}>
//         CONTINUE
//       </Button>
//     </Aux>
//   );
// };

// export default OrderSummary;
