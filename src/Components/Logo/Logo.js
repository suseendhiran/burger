import React from "react";
import burgerlogo from "../../../src/assets/images/burger-logo.png";
import styles from "./Logo.module.css";

function Logo() {
  return (
    <div className={styles.Logo}>
      <img src={burgerlogo} alt="logo"></img>
    </div>
  );
}

export default Logo;
