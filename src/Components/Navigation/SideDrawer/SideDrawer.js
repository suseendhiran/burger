import React from "react";
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems";
import styles from "./SideDrawer.module.css";
import Aux from "../../../hoc/Ax";
import BackDrop from "../../UI/BackDrop/BackDrop";

function SideDrawer(props) {
  let attachedStyles = [styles.sidedrawer, styles.close];
  if (props.open) {
    attachedStyles = [styles.sidedrawer, styles.open];
  }
  return (
    <Aux>
      <BackDrop show={props.open} clicked={props.closed} />
      <div className={attachedStyles.join(" ")} onClick={props.closed}>
        <div className={styles.logo}>
          <Logo />
        </div>

        <nav>
          <NavigationItems isAuthenticated={props.isAuth} />
        </nav>
      </div>
    </Aux>
  );
}

export default SideDrawer;
