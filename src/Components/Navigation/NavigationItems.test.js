import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { configure, shallow } from "enzyme";
import NavigationItems from "./NavigationItems";
import NavigationItem from "./NavigationItem/NavigationItem";

configure({ adapter: new Adapter() });

describe("testing Navigation items", () => {
  let wrapper;
  beforeEach(() => {
    wrapper = shallow(<NavigationItems />);
  });
  it("Should render two nav items if not authenticated", () => {
    wrapper.setProps({ isAuthenticated: false });
    expect(wrapper.find(NavigationItem)).toHaveLength(2);
  });
  it("Should render three nav items if  authenticated", () => {
    wrapper.setProps({ isAuthenticated: true });
    expect(wrapper.find(NavigationItem)).toHaveLength(3);
  });

  it("Checking presence of Logout", () => {
    wrapper.setProps({ isAuthenticated: true });
    expect(
      wrapper.contains(<NavigationItem link="./logout">LogOut</NavigationItem>)
    ).toBe(true);
  });
  it("Checking presence of orders", () => {
    wrapper.setProps({ isAuthenticated: true });
    expect(
      wrapper.contains(<NavigationItem link="./orders">Orders</NavigationItem>)
    ).toBe(true);
  });
  it("Checking presence of authenticate", () => {
    wrapper.setProps({ isAuthenticated: false });
    expect(
      wrapper.contains(
        <NavigationItem link="./auth">Authenticate</NavigationItem>
      )
    ).toBe(true);
  });
});
