import React from "react";
import styles from "./Toolbar.module.css";
import Logo from "../../Logo/Logo";
import NavigationItems from "../NavigationItems";
import DrawerToggle from "../SideDrawer/DrawerToggle/DrawerToggle";

function Toolbar(props) {
  return (
    <header className={styles.toolbar}>
      {/* <div onClick={props.showSidebar}>Menu</div> */}
      <DrawerToggle clicked={props.showSidebar} />
      <div className={styles.logo}>
        <Logo />
      </div>

      <nav className={styles.desktoponly}>
        <NavigationItems isAuthenticated={props.isAuth} />
      </nav>
    </header>
  );
}

export default Toolbar;
