import React from "react";
import PropTypes from "prop-types";

import styles from "./Ingredients.module.css";

function BurgerIngredients(props) {
  let ingredient = null;
  switch (props.type) {
    case "bread-bottom":
      ingredient = <div className={styles.BreadBottom}></div>;
      break;
    case "bread-top":
      ingredient = (
        <div className={styles.BreadTop}>
          <div className={styles.Seeds1}></div>
          <div className={styles.Seeds2}></div>
        </div>
      );
      break;
    case "meat":
      ingredient = <li className={styles.Meat}>{props.children}</li>;
      break;
    case "cheese":
      ingredient = <li className={styles.Cheese}>{props.children}</li>;
      break;
    case "bacon":
      ingredient = <li className={styles.Bacon}>{props.children}</li>;
      break;
    case "salad":
      ingredient = <li className={styles.Salad}>{props.children}</li>;
      break;
    default:
      ingredient = null;
  }
  return ingredient;
}

BurgerIngredients.propTypes = {
  type: PropTypes.string.isRequired,
};

export default BurgerIngredients;
