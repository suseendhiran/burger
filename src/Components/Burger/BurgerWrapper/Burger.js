import React from "react";
import { withRouter } from "react-router-dom";
import TransitionGroup from "react-transition-group/TransitionGroup";
import CSSTransition from "react-transition-group/CSSTransition";
import styles from "./Burger.module.css";
import BurgerIngredients from "../BurgerIngredients/BurgerIngredients";
import "./wrapper.css";

function Burger(props) {
  let ingredientsArray = Object.keys(props.ingredients)
    .map((igkey) => {
      return [...Array(props.ingredients[igkey])].map((_, i) => {
        return (
          <CSSTransition
            in={true}
            timeout={{ enter: 1000, exit: 600 }}
            classNames="fade"
            key={igkey + i}
          >
            <BurgerIngredients type={igkey}>{igkey}</BurgerIngredients>
          </CSSTransition>
        );
      });
    })
    .reduce((arr, el) => {
      return arr.concat(el);
    }, []);

  if (ingredientsArray.length === 0) {
    ingredientsArray = (
      <CSSTransition timeout={{ enter: 600 }}>
        <p>Please start adding ingredients!</p>
      </CSSTransition>
    );
  }

  return (
    <div className={styles.burgerwrapper}>
      <div className={styles.burger}>
        <BurgerIngredients type="bread-top" />
        <TransitionGroup component="ul" className={styles.list}>
          {ingredientsArray}
        </TransitionGroup>
        <BurgerIngredients type="bread-bottom" />
      </div>
    </div>
  );
}

export default withRouter(Burger);
