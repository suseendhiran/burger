import React from "react";
import styles from "./BuildControls.module.css";
import BuildControl from "./BuildControl/BuildControl";

const controls = [
  { label: "Salad", type: "salad" },
  { label: "Bacon", type: "bacon" },
  { label: "Cheese", type: "cheese" },
  { label: "Meat", type: "meat" },
];

function BuildControls(props) {
  return (
    <div className={styles.buildcontrols}>
      <div className={styles.controlswrapper}>
        <p>
          <strong className={styles.price}>
            Price : {props.price.toFixed(2)}
          </strong>
        </p>
        {controls.map((control) => {
          return (
            <BuildControl
              key={control.label}
              label={control.label}
              added={() => {
                props.addedIngredient(control.type);
              }}
              removed={() => {
                props.removedIngredient(control.type);
              }}
              type={control.type}
              disabled={props.disabled[control.type]}
            />
          );
        })}
        <button
          className={styles.OrderButton}
          disabled={!props.purchasable}
          onClick={props.ordered}
        >
          {props.isAuth ? "ORDER NOW" : "SIGN UP/ SIGN IN TO ORDER"}
        </button>
      </div>
    </div>
  );
}

export default BuildControls;
