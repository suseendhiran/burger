import React from "react";
import styles from "./Order.module.css";

function Order(props) {
  const ingredients = [];
  for (let ingname in props.ingredients) {
    ingredients.push({
      name: ingname,
      amount: props.ingredients[ingname],
    });
  }
  const ingredientsout = ingredients.map((ingredient) => {
    return (
      <span
        key={ingredient.name}
        style={{
          textTransform: "capitalize",
          display: "inline-block",
          margin: "0 8px",
          border: "1px solid #ccc",
          padding: "5px",
        }}
      >
        {ingredient.name}({ingredient.amount})
      </span>
    );
  });
  return (
    <div className={styles.order}>
      <p>Ingredients: {ingredientsout}</p>
      <p>
        Price: <strong>USD : {props.price}</strong>
      </p>
    </div>
  );
}

export default Order;
