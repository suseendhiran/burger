import React from "react";
import Burger from "../../Burger/BurgerWrapper/Burger";
import Button from "../../UI/Button/Button";
import styles from "./CheckoutSummary.module.css";

function CheckoutSummary(props) {
  return (
    <div className={styles.checkoutsummary}>
      <h1>Be Ready to Taste</h1>
      <div style={{ margin: "auto" }}>
        <Burger ingredients={props.ingredients} />
      </div>
      <Button btnType="Danger" clicked={props.cancelcheckout}>
        CANCEL
      </Button>
      <Button btnType="Success" clicked={props.continuecheckout}>
        CHECKOUT
      </Button>
    </div>
  );
}

export default CheckoutSummary;
