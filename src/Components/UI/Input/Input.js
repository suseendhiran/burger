import React from "react";
import styles from "./Inputt.module.css";

function Input(props) {
  let inputElement = null;

  switch (props.elementType) {
    case "authInput":
      inputElement = (
        <input
          {...props.elementConfig}
          className={styles.inputelement}
          value={props.value}
          onChange={props.changed}
        />
      );
      break;

    case "input":
      inputElement = (
        <div>
          <input
            {...props.elementConfig}
            className={styles.inputelement}
            // value={props.value}
            name={props.name}
            onChange={(e) => {
              props.formikHandle(props.name, e.target.value);
            }}
            onBlur={() => {
              props.handleBlur(props.name);
            }}
          />
          {props.error && props.touched ? (
            <p className={styles.error}>{props.error}</p>
          ) : null}
        </div>
      );
      break;

    case "select":
      inputElement = (
        <select
          className={styles.select}
          // value={props.value}
          onChange={(e) => {
            props.formikHandle(props.name, e.target.value);
          }}
        >
          {props.elementConfig.options.map((option) => {
            return (
              <option key={option.value} value={option.value}>
                {option.displayValue}
              </option>
            );
          })}
        </select>
      );
      break;
    default:
      inputElement = (
        <input
          className={styles.inputelement}
          {...props.elementConfig}
          value={props.value}
          onChange={props.changed}
        />
      );
  }
  return (
    <div className={styles.input}>
      <label className={styles.label}>{props.placeholder}</label>
      {inputElement}
    </div>
  );
}

export default Input;
