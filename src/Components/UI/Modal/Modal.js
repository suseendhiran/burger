import styles from "./Modal.module.css";
import Aux from "../../../hoc/Ax";
import BackDrop from "../BackDrop/BackDrop";

import React from "react";

export const Modal = (props) => {
  return (
    <Aux>
      <BackDrop show={props.show} clicked={props.cancelModal} />
      <div
        className={styles.Modal}
        style={{
          transform: props.show ? "translateY(0vh)" : "translateY(-100vh)",
          opacity: props.show ? "1" : "0",
        }}
      >
        {props.children}
      </div>
    </Aux>
  );
};

export default Modal;

// function Modal(props) {
//   return (
//     <Aux>
//       <BackDrop show={props.show} clicked={props.cancelModal} />
//       <div
//         className={styles.Modal}
//         style={{
//           transform: props.show ? "translateY(0vh)" : "translateY(-100vh)",
//           opacity: props.show ? "1" : "0",
//         }}
//       >
//         {props.children}
//       </div>
//     </Aux>
//   );
// }

// export default Modal;
