import { useState, useEffect } from "react";

export default (httpClient) => {
  const [error, setError] = useState(null);
  const request = httpClient.interceptors.request;
  const response = httpClient.interceptors.response;

  const reqInterceptor = request.use((req) => {
    setError(null);
    return req;
  });
  const resInterceptor = response.use(
    (res) => res,
    (err) => {
      setError(err);
    }
  );

  useEffect(() => {
    return () => {
      request.eject(reqInterceptor);
      response.eject(resInterceptor);
    };
  }, [reqInterceptor, resInterceptor, request, response]);

  const handleErrorConfirmed = () => {
    setError(null);
  };

  return [error, handleErrorConfirmed];
};
