import axios from "axios";

const instance = axios.create({
  baseURL: "https://react-myburger-f0395.firebaseio.com/",
});

export default instance;
