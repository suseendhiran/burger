import reducer from "./Auth";
import * as actionTypes from "../actions/actionTypes";

describe("testing auth reducer", () => {
  it("returns initial state when state is undefine", () => {
    expect(reducer(undefined, {})).toEqual({
      Idtoken: null,
      userId: null,
      error: null,
      loading: false,
      authRedirectPath: "/",
    });
  });
  it("changing respective states when auth is success", () => {
    expect(
      reducer(
        {
          Idtoken: null,
          userId: null,
          error: null,
          loading: false,
          authRedirectPath: "/",
        },
        {
          type: actionTypes.AUTH_SUCCESS,
          Idtoken: "IDtoken",
          userId: "userid",
          loading: false,
          error: null,
        }
      )
    ).toEqual({
      Idtoken: "IDtoken",
      userId: "userid",
      error: null,
      loading: false,
      authRedirectPath: "/",
    });
  });
});
