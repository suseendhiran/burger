import React from "react";

import Modal from "../../Components/UI/Modal/Modal";
import Aux from "../Ax";
import useHttpErrorHandler from "../../Hooks/httpErrorHandler";

const withErrorHandler = (WrappedComponent, axios) => {
  return (props) => {
    const [error, handleErrorConfirmed] = useHttpErrorHandler(axios);
    return (
      <Aux>
        <Modal show={error} cancelModal={handleErrorConfirmed}>
          {error ? error.message : null}
        </Modal>
        <WrappedComponent {...props} />
      </Aux>
    );
  };
};

export default withErrorHandler;
