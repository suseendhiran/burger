import React, { useState } from "react";
import { connect } from "react-redux";
import Aux from "../Ax";

import styles from "./Layout.module.css";
import Toolbar from "../../Components/Navigation/Toolbar/Toolbar";
import SideDrawer from "../../Components/Navigation/SideDrawer/SideDrawer";

function Layout(props) {
  const [showSideBar, setShowSideBar] = useState(false);

  const handleCloseSideBar = () => {
    setShowSideBar(false);
  };
  const handleOpenSideBar = () => {
    setShowSideBar(!showSideBar);
  };

  return (
    <Aux>
      <Toolbar isAuth={props.isAuthenticated} showSidebar={handleOpenSideBar} />
      <SideDrawer
        isAuth={props.isAuthenticated}
        open={showSideBar}
        closed={handleCloseSideBar}
      />
      <main className={styles.content}>{props.children}</main>
    </Aux>
  );
}
const mapStateToProps = (state) => {
  return {
    isAuthenticated: state.auth.Idtoken !== null,
  };
};

export default connect(mapStateToProps)(Layout);
