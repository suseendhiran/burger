import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useFormik } from "formik";

import Input from "../../Components/UI/Input/Input";
import Button from "../../Components/UI/Button/Button";
import styles from "./AuthForm.module.css";
import * as actions from "../../hoc/Store/actions/index";
import Spinner from "../../Components/UI/Spinner/Spinner";
import { authValidationSchema } from "../../ValidationSchemas/validationSchema";
import { Redirect } from "react-router-dom";

const initialValues = {
  email: "",
  password: "",
};

export const AuthForm = (props) => {
  const onSubmit = (values) => {
    props.onAuth(values.email, values.password, isSignUp);
  };

  const formik = useFormik({
    initialValues: initialValues,
    onSubmit: onSubmit,
    validationSchema: authValidationSchema,
  });
  const [controls] = useState({
    email: {
      elementType: "input",
      elementConfig: {
        type: "email",
        placeholder: "Mail Address",
        name: "email",
      },
      value: "",
    },
    password: {
      elementType: "input",
      elementConfig: {
        type: "password",
        placeholder: "Password",
        name: "password",
        autoComplete: "on",
      },
      value: "",
    },
  });
  const [isSignUp, setIsSignUp] = useState(true);
  const { buildingBurger, authRedirectPath, onSetAuthRedirectPath } = props;

  useEffect(() => {
    if (!buildingBurger && authRedirectPath !== "/") {
      onSetAuthRedirectPath();
    }
  }, [buildingBurger, onSetAuthRedirectPath, authRedirectPath]);

  const handleSwitchToLogin = () => {
    setIsSignUp(!isSignUp);
  };

  let formElements = [];
  for (let key in controls) {
    formElements.push({
      id: key,
      config: controls[key],
    });
  }
  let form = formElements.map((formElement) => (
    <Input
      key={formElement.id}
      elementType={formElement.config.elementType}
      elementConfig={formElement.config.elementConfig}
      value={formElement.config.value}
      name={formElement.config.elementConfig.name}
      formikHandle={formik.setFieldValue}
      error={formik.errors[formElement.config.elementConfig.name]}
      handleBlur={formik.setFieldTouched}
      touched={formik.touched[formElement.config.elementConfig.name]}
    />
  ));
  if (props.loading) {
    form = <Spinner />;
  }
  let errorMessage = null;
  if (props.error) errorMessage = <p>{props.error.message}</p>;
  let authRedirect = null;
  if (props.isAuthenticated) {
    authRedirect = <Redirect to={props.authRedirectPath} />;
  }
  return (
    <div className={styles.Auth}>
      {authRedirect}
      <h2>{isSignUp ? "SIGN UP" : "SIGN IN"}</h2>
      {errorMessage}
      <form onSubmit={formik.handleSubmit}>
        {form}
        <Button btnType="Success" type="submit">
          SUBMIT
        </Button>
      </form>
      <Button btnType="Danger" clicked={handleSwitchToLogin}>
        SWITCH TO {!isSignUp ? "SIGN UP" : "SIGN IN"}
      </Button>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAuth: (email, password, isSignUp) =>
      dispatch(actions.auth(email, password, isSignUp)),
    onSetAuthRedirectPath: () => dispatch(actions.setAuthRedirectPath("/")),
  };
};

const mapStateToProps = (state) => {
  return {
    loading: state.auth.loading,
    error: state.auth.error,
    isAuthenticated: state.auth.Idtoken != null,
    buildingBurger: state.burgerBuilder.building,
    authRedirectPath: state.auth.authRedirectPath,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(AuthForm);
