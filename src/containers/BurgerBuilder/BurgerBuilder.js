import React, { useEffect, useState, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import Aux from "../../hoc/Ax";
import Burger from "../../Components/Burger/BurgerWrapper/Burger";
import BuildControls from "../../Components/Burger/BuildControls/BuildControls";
import Modal from "../../Components/UI/Modal/Modal";
import OrderSummary from "../../Components/OrderSummary/OrderSummary";
import axios from "../../../src/axios-orders";
import Spinner from "../../Components/UI/Spinner/Spinner";
import withErrorHandler from "../../hoc/WithErrorHandler/WithErrorHandler";
import * as actions from "../../hoc/Store/actions/index";

const BurgerBuilder = (props) => {
  const [purchasing, setPurchasing] = useState(false);

  const dispatch = useDispatch();

  const ings = useSelector((state) => state.burgerBuilder.ingredients);
  const totPrice = useSelector((state) => state.burgerBuilder.totalPrice);
  const error = useSelector((state) => state.burgerBuilder.error);
  const isAuthenticated = useSelector((state) => state.auth.Idtoken);

  const onIngredientsAdded = (ingName) =>
    dispatch(actions.addIngredient(ingName));
  const onIngredientsRemoved = (ingName) =>
    dispatch(actions.removeIngredient(ingName));
  const onInitIngredients = useCallback(
    () => dispatch(actions.initIngredients()),
    [dispatch]
  );
  const onInitPurchase = () => dispatch(actions.purchaseInit());
  const onSetAuthRedirectPath = (path) =>
    dispatch(actions.setAuthRedirectPath(path));
  useEffect(() => {
    onInitIngredients();
  }, [onInitIngredients]);

  const updatePurchaseState = (purchaseIngredients) => {
    const sum = Object.values(purchaseIngredients)
      // .map((key) => {
      //   return purchaseIngredients[key];
      // })
      .reduce((sum, e) => {
        return (sum = sum + e);
      }, 0);
    return sum > 0;
  };

  const handleOrder = () => {
    if (isAuthenticated) {
      setPurchasing(true);
    } else {
      onSetAuthRedirectPath("/checkout");
      props.history.push("/auth");
    }
  };
  const handleCancelModal = () => {
    setPurchasing(false);
  };
  const handleCancel = () => {
    setPurchasing(false);
  };
  const handleContinue = () => {
    onInitPurchase();
    props.history.push({
      pathname: "/checkout",
    });
  };

  const disabledInfo = {
    ...ings,
  };
  for (let key in disabledInfo) {
    disabledInfo[key] = disabledInfo[key] <= 0;
  }
  let orderSummary = <Spinner />;

  let burger = error ? <p>Ingredients can't be loaded</p> : <Spinner />;
  if (ings) {
    burger = (
      <Aux>
        <Burger ingredients={ings} />
        <BuildControls
          addedIngredient={onIngredientsAdded}
          removedIngredient={onIngredientsRemoved}
          disabled={disabledInfo}
          price={totPrice}
          purchasable={updatePurchaseState(ings)}
          ordered={handleOrder}
          isAuth={isAuthenticated}
        />
      </Aux>
    );

    orderSummary = (
      <OrderSummary
        ingredients={ings}
        cancelled={handleCancel}
        continued={handleContinue}
        price={totPrice}
      />
    );
  }

  return (
    <Aux>
      <Modal show={purchasing} cancelModal={handleCancelModal}>
        {orderSummary}
      </Modal>
      {burger}
    </Aux>
  );
};

export default withErrorHandler(BurgerBuilder, axios);
