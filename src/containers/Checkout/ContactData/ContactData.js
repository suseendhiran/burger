import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import { useFormik } from "formik";
import Button from "../../../Components/UI/Button/Button";
import styles from "./ContactData.module.css";
import axios from "../../../axios-orders";
import Spinner from "../../../Components/UI/Spinner/Spinner";
import Aux from "../../../hoc/Ax";
import withErrorHandler from "../../../hoc/WithErrorHandler/WithErrorHandler";
import Input from "../../../Components/UI/Input/Input";
import * as actions from "../../../hoc/Store/actions/index";
import { contactDataValidationSchema } from "../../../ValidationSchemas/validationSchema";

const initialValues = {
  name: "",
  email: "",
  street: "",
  postalcode: "",
  country: "",
  deliveryMethod: "fastest",
};

export const ContactData = (props) => {
  const onSubmit = (values) => {
    console.log("Form data", values);
    const contactData = values;
    // for (let element in orderForm) {
    //   contactData[element] = orderForm[element].value;
    // }
    const order = {
      ingredients: props.ings,
      price: props.totPrice,
      contactinfo: contactData,
      userId: props.userId,
    };
    props.onBurgerOrder(order, props.token);
  };
  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema: contactDataValidationSchema,
  });
  useEffect(() => {});
  const [orderForm] = useState({
    name: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Your Name",
        name: "name",
      },
      error: formik.errors.name,
      value: formik.values.name,
    },
    email: {
      elementType: "input",
      elementConfig: {
        type: "email",
        placeholder: "Your Mail",
        name: "email",
      },
      value: formik.values.email,
      error: formik.errors.email,
    },
    street: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Your Street",
        name: "street",
      },
      value: formik.values.street,
      error: formik.errors.street,
    },
    postalcode: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "ZIP code",
        name: "postalcode",
      },
      value: formik.values.postalcode,
      error: formik.errors.postalcode,
    },
    country: {
      elementType: "input",
      elementConfig: {
        type: "text",
        placeholder: "Country",
        name: "country",
      },
      value: formik.values.country,
      error: formik.errors.country,
    },
    deliveryMethod: {
      elementType: "select",
      elementConfig: {
        options: [
          { value: "fastest", displayValue: "Fastest" },
          { value: "cheapest", displayValue: "Cheapest" },
        ],
        name: "deliveryMethod",
      },
      value: formik.values.deliveryMethod,
      error: formik.errors.deliveryMethod,
    },
  });

  let formElements = [];
  for (let key in orderForm) {
    formElements.push({
      id: key,
      config: orderForm[key],
    });
  }
  let form = (
    <Aux>
      <h4>Enter your contact data</h4>
      <form className={styles.form} onSubmit={formik.handleSubmit}>
        {formElements.map((element) => (
          <Input
            key={element.id}
            elementType={element.config.elementType}
            elementConfig={element.config.elementConfig}
            value={element.config.value}
            name={element.config.elementConfig.name}
            formikHandle={formik.setFieldValue}
            error={formik.errors[element.config.elementConfig.name]}
            handleBlur={formik.setFieldTouched}
            touched={formik.touched[element.config.elementConfig.name]}
          />
        ))}

        <Button btnType="Success" type="submit">
          ORDER
        </Button>
      </form>
    </Aux>
  );
  if (props.loading) {
    form = <Spinner />;
  }

  return <div className={styles.contactdata}>{form}</div>;
};

const mapStateToProps = (state) => {
  return {
    ings: state.burgerBuilder.ingredients,
    totPrice: state.burgerBuilder.totalPrice,
    loading: state.order.loading,
    token: state.auth.Idtoken,
    userId: state.auth.userId,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onBurgerOrder: (orderData, token) =>
      dispatch(actions.purchaseBurger(orderData, token)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withErrorHandler(ContactData, axios));
