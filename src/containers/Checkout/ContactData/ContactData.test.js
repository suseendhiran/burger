import React from "react";
import Adapter from "enzyme-adapter-react-16";
import { configure, shallow } from "enzyme";
import { ContactData } from "./ContactData";
import Spinner from "../../../Components/UI/Spinner/Spinner";
import Input from "../../../Components/UI/Input/Input";
import Button from "../../../Components/UI/Button/Button";

configure({ adapter: new Adapter() });

describe("testing contact data comp", () => {
  let wrapper = shallow(<ContactData />);
  it("check spinner rendering", () => {
    wrapper.setProps({ loading: true });
    expect(wrapper.find(Spinner)).toHaveLength(1);
  });
  it("check no of inputs", () => {
    wrapper.setProps({ loading: false });
    expect(wrapper.find(Input)).toHaveLength(6);
  });
  it("check button rendering", () => {
    expect(wrapper.find(Button)).toHaveLength(1);
  });
});
