import * as Yup from "yup";

export const contactDataValidationSchema = Yup.object({
  name: Yup.string().required("Name required"),
  email: Yup.string().email("Invaid Email format").required("Email required"),
  street: Yup.string().required("Street name required"),
  postalcode: Yup.string().required("Postal code required "),
  country: Yup.string().required("Country name required"),
});

export const authValidationSchema = Yup.object({
  email: Yup.string()
    .email("Invalid Email format")
    .required("Email ID required to Sign in/Sign up"),
  password: Yup.string().required("Password Required"),
});
